package pl.planito.bl.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.planito.bl.service.transformer.Transformer;
import pl.planito.dto.HardwareDto;
import pl.planito.entities.Hardware;
import pl.planito.repositories.HardwareRepository;

@Service
public class HardwareServiceImpl implements HardwareService {

	@Autowired
	private HardwareRepository hardwareRepository;

	@Override
	public List<HardwareDto> getHardwares() {
		List<Hardware> hardwares = hardwareRepository.findAll();
		List<HardwareDto> hardwareDtos = new ArrayList<HardwareDto>();
		for (Hardware hardware : hardwares) {
			HardwareDto hardwareDto = new HardwareDto();
			Transformer<Hardware, HardwareDto> transforme = new Transformer<Hardware, HardwareDto>(
					hardware, hardwareDto);
			hardwareDtos.add(transforme.getOutClass());
		}
		return hardwareDtos;
	}
}
