package pl.planito.bl.service;

import pl.planito.entities.User;

/**
 * 
 * @author yaotzin
 *
 */
public interface UserService {
	/**
	 * Adding a user to database
	 */
	void add();

	/**
	 * Removing a specified user
	 */
	void remove();

	/**
	 * Find User if exists, return also related person data
	 * 
	 * @param id
	 *            id of user
	 * @return
	 */
	User find(final Integer id);
}
