package pl.planito.bl.service;

import java.util.List;

import pl.planito.dto.HardwareDto;

public interface HardwareService {
	List<HardwareDto> getHardwares();
}
