package pl.planito.bl.service.transformer;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

public class Transformer<I, O> {
	private I inClass;
	private O outClass;
	private Logger logger = LoggerFactory.getLogger(Transformer.class);

	public Transformer(I inClass, O outClass) {
		this.inClass = inClass;
		this.outClass = outClass;
		copyFields();
	}

	public O getOutClass() {
		return this.outClass;
	}

	private List<Field> getOutFields() {
		List<Field> fields = Arrays.asList(outClass.getClass()
				.getDeclaredFields());
		return fields;
	}

	private void copyFields() {
		List<Field> outFields = getOutFields();
		BeanWrapper inClassWrapper = new BeanWrapperImpl(inClass);
		BeanWrapper outClassWrapper = new BeanWrapperImpl(outClass);

		for (Field outField : outFields) {
			try {
				Object val = inClassWrapper
						.getPropertyValue(outField.getName());
				outClassWrapper.setPropertyValue(outField.getName(), val);
			} catch (IllegalArgumentException e) {
				logger.debug(e.getMessage(), e);
			}
		}
	}
}
