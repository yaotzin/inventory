package pl.planito.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.planito.entities.Hardware;

public interface HardwareRepository extends JpaRepository<Hardware, Long> {

}
