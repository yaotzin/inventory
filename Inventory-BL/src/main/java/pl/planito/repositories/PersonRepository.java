package pl.planito.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.planito.entities.Person;

public interface PersonRepository extends JpaRepository<Person, Long> {

}
