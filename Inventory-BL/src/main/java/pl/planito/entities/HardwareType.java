package pl.planito.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * Entity contain Hardware type definitions
 * 
 * @author yaotzin
 *
 */
@Entity
@Table(name = "hardware_type")
public class HardwareType {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(nullable = false, length = 300, unique = true)
	private String name;

	@Column(nullable = true, length = 1500)
	private String description;

	public HardwareType() {
	}

	/**
	 * get auto generated Id of hardware type
	 * 
	 * @return database id of hardware type
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return Name of hardware type
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 *            Name of hardware type
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * get the descriptoon of hardware type
	 * 
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}
