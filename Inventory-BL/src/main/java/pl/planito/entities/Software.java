package pl.planito.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "software")
public class Software {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(length = 250)
	private String name;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "software_software_type", joinColumns = { @JoinColumn(name = "software_id") }, inverseJoinColumns = { @JoinColumn(name = "software_type_id") })
	private Set<SoftwareType> softwareTypes = new HashSet<SoftwareType>();

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "software_software_licence", joinColumns = { @JoinColumn(name = "software_id") }, inverseJoinColumns = { @JoinColumn(name = "software_type_id") })
	private Set<SoftwareLicence> softwareLicences;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private User user;

	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<SoftwareType> getSoftwareTypes() {
		return softwareTypes;
	}

	public void setSoftwareTypes(Set<SoftwareType> softwareTypes) {
		this.softwareTypes = softwareTypes;
	}

	public Set<SoftwareLicence> getSoftwareLicences() {
		return softwareLicences;
	}

	public void setSoftwareLicences(Set<SoftwareLicence> softwareLicences) {
		this.softwareLicences = softwareLicences;
	}

	public Software() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
