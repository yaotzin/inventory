package pl.planito.entities;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "software_type")
public class SoftwareType {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(length = 50)
	private String name;
	@Column(length = 1500)
	private String description;

	@ManyToMany(mappedBy = "softwareTypes", fetch = FetchType.LAZY)
	private Set<Software> softwares;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<Software> getSoftwares() {
		return softwares;
	}

	public void setSoftwares(Set<Software> softwares) {
		this.softwares = softwares;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
