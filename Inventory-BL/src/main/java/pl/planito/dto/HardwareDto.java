package pl.planito.dto;

public class HardwareDto {
	private Long id;
	private String hardwareName;
	private String serialNumber;
	private String description;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getHardwareName() {
		return hardwareName;
	}

	public void setHardwareName(String hardwareName) {
		this.hardwareName = hardwareName;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static class Builder {
		HardwareDto build;

		public Builder(Long id, String hardwareName, String serialNumber,
				String description) {
			build = new HardwareDto();
			build.setId(id);
			build.setHardwareName(hardwareName);
			build.setSerialNumber(serialNumber);
			build.setDescription(description);
		}

		public HardwareDto build() {
			return build;
		}
	}

}
