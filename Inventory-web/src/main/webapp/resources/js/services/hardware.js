inventoryApp.service('HardwareService', ['$http','$q', function($http, $q){
	this.getHardwareTableData=function(){
		var deferred = $q.defer();
		 
        $http.get('/rest/hardware-table')
            .then(function (response) {
                if (response.status == 200) {
                   deferred.resolve(response.data);
                }
                else {
                    deferred.reject('Error retrieving hardware data');
                }
        });
        
        return deferred.promise;
	}
	
	this.addHardwareRow = function(){
		
	}
}]);