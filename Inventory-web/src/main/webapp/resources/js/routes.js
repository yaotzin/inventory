inventoryApp.config(['$routeProvider',
                    function($routeProvider) {
                      $routeProvider.
                        when('/hardware', {
                          templateUrl: '/hardware/',
                          controller: 'HardwareController'
                        }).
                        when('/software',{
                        	templateUrl: '/software/',
                        	controller: 'SoftwareController'
                        }).
                        when('/user',{
                        	templateUrl:'/user/',
                        	controller: 'UserController'
                        })
                    }]);