inventoryApp.controller('HardwareAddDialogController',function($scope, $modal, $log) {
	$scope.hardware = {
			name:'test',
			serialNumber:'123',
			descritption:'rrrr'
	};
	$scope.open = function (size) {

	    var modalInstance = $modal.open({
	      templateUrl: '/hardware/add-hardware',
	      controller: 'HardwareModalInstanceController',
	      size: size,
	      resolve: {
	        items: function () {
	          return [1,2,3];
	        }
	      }
	    });

	    modalInstance.result.then(function (selectedItem) {
	      $scope.selected = selectedItem;
	    }, function () {
	      $log.info('Modal dismissed at: ' + new Date());
	    });
	  };
    
});

inventoryApp.controller('HardwareModalInstanceController', function ($scope, $modalInstance, items) {

	  $scope.items = items;

	  $scope.ok = function () {
	    console.log($scope);
		  //$modalInstance.close($scope.selected.item);
	  };

	  $scope.cancel = function () {
	    $modalInstance.dismiss('cancel');
	  };
	});