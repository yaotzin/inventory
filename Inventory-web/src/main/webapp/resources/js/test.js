var testApp = angular.module('InventoryApp', ['ui.bootstrap', 'ngRoute']);

testApp.controller('TestController',function($scope, $http) {
	$scope.myMethod = function(){
     $http.get('/test/json').
        success(function(data) {
            $scope.tests = data;
        });
	}
    
});

testApp.controller('TabsDemoCtrl', function ($scope) {
	  $scope.tabs = [
	    { title:'Dynamic Title 1', content:'Dynamic content 1' },
	    { title:'Dynamic Title 2', content:'Dynamic content 2', disabled: true }
	  ];
	  $scope.alertMe = function() {
	    setTimeout(function() {
	      alert('You\'ve selected the alert tab!');
	    });
	  };
	});

testApp.config(['$routeProvider',
                    function($routeProvider) {
                      $routeProvider.
                        when('/test/layout', {
                          templateUrl: '/test/layout',
                          controller: 'TestController'
                        })
                    }]);