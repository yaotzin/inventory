<div ng-controller="HardwareAddDialogController">
	<div class="btn-group">
		<a ng-click="open()" role="button" class="btn btn-primary">Define
			new hardware</a>
	</div>
</div>
<div class="row">
	<div class="btn-group"></div>
</div>
<hr />
<div ng-controller="HardwareController">
	<table class="table table-stripped">
		<thead>
			<tr>
				<th>Serial number</th>
				<th>Name</th>
				<th>Description</th>
			</tr>
		</thead>
		<tbody>
			<tr ng-repeat="hardware in hardwares">
				<td>{{hardware.serialNumber}}</td>
				<td>{{hardware.hardwareName}}</td>
				<td>{{hardware.description}}</td>
			</tr>
		</tbody>
	</table>
</div>