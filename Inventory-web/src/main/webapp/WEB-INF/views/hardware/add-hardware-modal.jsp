
        <div class="modal-header">
            <h3 class="modal-title">Add hardware</h3>
        </div>
        <div class="modal-body">
        <form id="hardwareAddForm" novalidate class="form-horizontal well" role="form">
        	<div class="form-group">
        		<label class="form-label" for="hardwareName">Hardware name:</label>
        		<input type="text" id="hardwareName" ng-model="hardware.name" class="form-control" name="hardwareName" placeholder="Enter hardware name"/>
        	</div>
        	<div class="form-group">
        		<label for="serialNumber">Serial number:</label>
        		<input type="text" id="serialNumber" ng-model="hardware.serialNumber" class="form-control" name="serialNumber" placeholder="Enter serial number"/>
        	</div>
        	<div class="form-group">
        		<label for="description">Description:</label>
        		<textarea id="description" class="form-control" ng-model="hardware.description" name="description" placeholder="Enter hardware name"></textarea>
        	</div>
        </form>
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" ng-click="ok()">OK</button>
            <button class="btn btn-warning" ng-click="cancel()">Cancel</button>
        </div>
  


