<%@ page session="false"%>
<!doctype html>
<html ng-app="InventoryApp">
<head>
<title>Inventory</title>
<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.10/angular.js"></script>
<script type="text/javascript"
	src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.18/angular-route.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/ui-bootstrap-tpls-0.11.0.min.js"></script>
<link rel="stylesheet"
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" />

<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/controller/home.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/controller/hardware.js"></script>
	<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/services/hardware.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/controller/user.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/controller/software.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/controller/hardware/hardwareAddDlg.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/routes.js"></script>
</head>
<body>
	<div class="container">
		<div class="page-header">
			<h1>Inventory</h1>
		</div>
		<div class="row">
			<div class="btn-group">
				<a href="#/home" role="button" class="btn btn-primary">Home</a> <a
					href="#/software" role="button" class="btn btn-primary">Software</a>
				<a href="#/hardware" role="button" class="btn btn-primary">Hardware</a>
				<a href="#/user" role="button" class="btn btn-primary">User</a>
			</div>
		</div>
		<hr />
		<div class="row">
			<div ng-view></div>
		</div>
	</div>
</body>
</html>
