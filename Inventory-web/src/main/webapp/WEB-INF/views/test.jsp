
		

		<div ng-controller="TabsDemoCtrl">
			<tabset> <tab heading="Static title">Static content</tab> <tab
				ng-repeat="tab in tabs" heading="{{tab.title}}" active="tab.active"
				disabled="tab.disabled"> {{tab.content}} </tab> <tab
				select="alertMe()"> <tab-heading> <i
				class="glyphicon glyphicon-bell"></i> Alert! </tab-heading> I've got an HTML
			heading, and a select callback. Pretty cool! </tab> </tabset>
		</div>

		<div ng-controller="TestController" ng-init="myMethod()">
			<div class="row">
				<div class="col-md-2">
					Search: <input ng-model="query" />
				</div>
			</div>
			<div ng-repeat="test in tests | filter:query" class="row">
				<div class="col-md-6">The random value is: {{test}}</div>
			</div>
		</div>
	</div>