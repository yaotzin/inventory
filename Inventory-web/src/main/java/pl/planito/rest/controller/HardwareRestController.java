package pl.planito.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import pl.planito.bl.service.HardwareService;
import pl.planito.dto.HardwareDto;

@RestController
@RequestMapping("/rest")
public class HardwareRestController {
	@Autowired
	HardwareService hardwareService;

	@RequestMapping("/hardware-table")
	@ResponseStatus(value = HttpStatus.OK)
	public List<HardwareDto> getHardwareTable() {
		List<HardwareDto> hardwares = hardwareService.getHardwares();
		return hardwares;
	}

	@RequestMapping(value = "/add-hardware", method = RequestMethod.POST)
	public void addHardware(@RequestBody HardwareDto data) {

	}
}
