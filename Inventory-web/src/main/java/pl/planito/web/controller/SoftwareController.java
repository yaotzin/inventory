package pl.planito.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/software")
public class SoftwareController {
	@RequestMapping("/")
	public String getInventrorySoftwarePage() {
		return "software";
	}
}
