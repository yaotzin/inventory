package pl.planito.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/user")
@Controller
public class UserController {
	@RequestMapping("/")
	public String getInventoryUserPage() {
		return "user";
	}
	
	@RequestMapping("/add-position")
	public String getDefinePositionPage(){
		return "user/add-position-modal";
	}
}
