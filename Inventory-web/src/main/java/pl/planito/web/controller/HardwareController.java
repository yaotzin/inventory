package pl.planito.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/hardware")
public class HardwareController {

	@RequestMapping("/")
	public String getInventroryHardwarePage() {

		return "hardware";
	}

	@RequestMapping("/add-hardware")
	public String getDefinePositionPage() {
		return "hardware/add-hardware-modal";
	}
}
